<#--
/****************************************************
 * Description: t_mall_order_item的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/order/item/save" id=tabId>
   <input type="hidden" name="id" value="${orderItem.id}"/>
   
   <@formgroup title='商品id'>
	<input type="text" name="itemId" value="${orderItem.itemId}" check-type="required">
   </@formgroup>
   <@formgroup title='订单id'>
	<input type="text" name="orderId" value="${orderItem.orderId}" check-type="required">
   </@formgroup>
   <@formgroup title='商品购买数量'>
	<input type="text" name="num" value="${orderItem.num}" check-type="number">
   </@formgroup>
   <@formgroup title='商品标题'>
	<input type="text" name="title" value="${orderItem.title}" >
   </@formgroup>
   <@formgroup title='商品单价'>
	<input type="text" name="price" value="${orderItem.price}" >
   </@formgroup>
   <@formgroup title='商品总金额'>
	<input type="text" name="totalFee" value="${orderItem.totalFee}" >
   </@formgroup>
   <@formgroup title='商品图片地址'>
	<input type="text" name="picPath" value="${orderItem.picPath}" >
   </@formgroup>
</@input>